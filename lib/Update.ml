open Monoid

(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-37-39"]

module Make (P : Monoid) (S : MonoidAction with type m = P.t) = struct
  module Reader = Reader.Make (struct
    type t = S.t
  end)

  module Writer = Writer.Make (P)

  let lift ((l1, k) : 'a Reader.t Writer.t) : 'a Writer.t Reader.t =
    failwith "NYI"


  module Base = struct
    type 'a t = 'a Writer.t Reader.t

    let return a = failwith "NYI"

    let bind m f = failwith "NYI"
  end

  module M = Monad.Expand (Base)
  include M
  open Base

  let get () = failwith "NYI"

  let set s = failwith "NYI"

  let run m s0 = failwith "NYI"
end

(* Want more? [http://cs.ioc.ee/~tarmo/papers/types13.pdf] *)
