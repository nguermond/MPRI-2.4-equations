(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-34-37-39"]

open Functor

module Make (F : Functor) = struct
  module Base =
    struct
      type 'a t =
        | Return of 'a
        | Op of 'a t F.t

      let return (x : 'a) : 'a t = Return x

      let rec bind (m : 'a t) (f : 'a -> 'b t) : 'b t =
        (match m with
         | Return x -> f x
         | Op w -> Op (F.map (fun x -> bind x f) w))

    end

  module M = Monad.Expand (Base)
  include M
  open Base

  let op (xs : 'a F.t) : 'a t = Op (F.map return xs)

  type ('a, 'b) algebra =
    { return : 'a -> 'b;
      op : 'b F.t -> 'b
    }

  let rec run (alg : ('a, 'b) algebra) (m : 'a t) : 'b =
    (match m with
     | Return x -> alg.return x
     | Op w -> alg.op (F.map (run alg) w))

end
